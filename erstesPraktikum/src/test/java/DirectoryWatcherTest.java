import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.util.logging.FileHandler;
import java.util.logging.SimpleFormatter;

import static org.junit.Assert.*;

public class DirectoryWatcherTest {
    DirectoryWatcher Test;
    FileHandler handler;


    @Before
    public  void setup(){
        Test=new DirectoryWatcher(System.getProperty("user.dir"));
    }


    @Test
    public void neuesFileEvent() throws InterruptedException {
        try {
            handler = new FileHandler(WatchedDirectory.class.getName() + ".log");
        } catch (IOException e) {
            e.printStackTrace();
            handler.setFormatter(new SimpleFormatter());
            DirectoryWatcher.logger.addHandler(handler);
            DirectoryWatcher.logger.setUseParentHandlers(false);
        }
      WatchKey watchKey=Test.getWatchService().take();
      WatchEvent watchEvent= watchKey.pollEvents().get(0);
     FileEvent test= Test.neuesFileEvent(watchEvent);
     assertEquals(watchEvent.kind().name(),"ENTRY_"+test.getSymbol().name());

    }

    @Test
    public void main() throws InterruptedException {
       String [] parameter =new String[] {System.getProperty("user.dir")};
       DirectoryWatcher.main(parameter);
      Thread.sleep(11000);
    }
}