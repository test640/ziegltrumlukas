import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;
import java.time.Instant;
import java.util.Date;
public class WatchedFileTest {
    WatchedFile Test;


    @Before
    public  void setup(){
         Test=new WatchedFile("dateiname","verzeichnisname");
    }
    @Test
    public void getCurrentState() {
        Assert.assertEquals(WatchedFile.State.CREATED, Test.getCurrentState());//Start
        Test.transition(WatchedFile.Symbol.CREATE);//Created-->Created
        Assert.assertEquals(WatchedFile.State.CREATED, Test.getCurrentState());
        Test.transition(WatchedFile.Symbol.MODIFY);//Created-->Created
        Assert.assertEquals(WatchedFile.State.CREATED, Test.getCurrentState());
        Test.transition(WatchedFile.Symbol.SYNC);//Created-->Insync
        Assert.assertEquals(WatchedFile.State.INSYNC, Test.getCurrentState());
        Test.transition(WatchedFile.Symbol.SYNC);//Insync-->Insync
        Assert.assertEquals(WatchedFile.State.INSYNC, Test.getCurrentState());
        Test.transition(WatchedFile.Symbol.CREATE);//Insync-->Modified
        Assert.assertEquals(WatchedFile.State.MODIFIED, Test.getCurrentState());
        Test.transition(WatchedFile.Symbol.SYNC);//Modified-->Insync
        Assert.assertEquals(WatchedFile.State.INSYNC, Test.getCurrentState());
        Test.transition(WatchedFile.Symbol.MODIFY);//Insync-->Modified
        Assert.assertEquals(WatchedFile.State.MODIFIED, Test.getCurrentState());
        Test.transition(WatchedFile.Symbol.CREATE);//Modified-->Modified
        Assert.assertEquals(WatchedFile.State.MODIFIED, Test.getCurrentState());
        Test.transition(WatchedFile.Symbol.MODIFY);//Modified-->Modified
        Assert.assertEquals(WatchedFile.State.MODIFIED, Test.getCurrentState());
        Test.transition(WatchedFile.Symbol.DELETE);//Modified-->Deleted
        Assert.assertEquals(WatchedFile.State.DELETED, Test.getCurrentState());
        Test.transition(WatchedFile.Symbol.CREATE);//Deleted-->Modified
        Assert.assertEquals(WatchedFile.State.MODIFIED, Test.getCurrentState());
        Test.transition(WatchedFile.Symbol.DELETE);//Modified-->Deleted
        Test.transition(WatchedFile.Symbol.MODIFY);//Deleted-->Modified
        Assert.assertEquals(WatchedFile.State.MODIFIED, Test.getCurrentState());
        Test.transition(WatchedFile.Symbol.SYNC);//Modified-->Insync
        Test.transition(WatchedFile.Symbol.DELETE);//Insync-->Deleted
        Assert.assertEquals(WatchedFile.State.DELETED, Test.getCurrentState());
        Test.transition(WatchedFile.Symbol.DELETE);//Deleted-->Deleted
        Assert.assertEquals(WatchedFile.State.DELETED, Test.getCurrentState());
        Test.transition(WatchedFile.Symbol.SYNC);//Deleted->Gone
        Assert.assertEquals(WatchedFile.State.GONE, Test.getCurrentState());
        Test.transition(WatchedFile.Symbol.SYNC);//Gone->Gone
        Assert.assertEquals(WatchedFile.State.GONE, Test.getCurrentState());
        Test.transition(WatchedFile.Symbol.DELETE);//Gone-->Gone
        Assert.assertEquals(WatchedFile.State.GONE, Test.getCurrentState());
        Test.transition(WatchedFile.Symbol.MODIFY);//Gone-->Gone
        Assert.assertEquals(WatchedFile.State.GONE, Test.getCurrentState());
        Test.transition(WatchedFile.Symbol.CREATE);//Gone-->Created
        Assert.assertEquals(WatchedFile.State.CREATED, Test.getCurrentState());
        Test.transition(WatchedFile.Symbol.DELETE);//Created-->Gone
        Assert.assertEquals(WatchedFile.State.GONE, Test.getCurrentState());


    }

    @Test
    public void getLastTimestamp()  {


        Test.transition(WatchedFile.Symbol.CREATE);
        Assert.assertEquals(Test.getLastTimstamp(),Date.from(Instant.now()));

    }




}