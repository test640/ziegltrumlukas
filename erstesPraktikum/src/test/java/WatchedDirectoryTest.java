import org.junit.Before;
import org.junit.Test;

import java.io.*;

import static org.junit.Assert.*;

public class WatchedDirectoryTest {
    WatchedDirectory Test;
    @Before
    public  void setup(){
        Test=new WatchedDirectory("verzeichnisname");
    }

    @Test
    public void update() {
        FileEvent fileEvent=new FileEvent("test", WatchedFile.Symbol.CREATE);
        Test.update(fileEvent);
        assertEquals(WatchedFile.State.CREATED,Test.getMap().get(fileEvent.getFilename()).getCurrentState());
        FileEvent fileEvent2=new FileEvent("test", WatchedFile.Symbol.SYNC);
        Test.update(fileEvent2);
        assertEquals(WatchedFile.State.INSYNC,Test.getMap().get(fileEvent2.getFilename()).getCurrentState());
        FileEvent fileEvent3=new FileEvent("test", WatchedFile.Symbol.DELETE);
        Test.update(fileEvent3);
        assertEquals(WatchedFile.State.DELETED,Test.getMap().get(fileEvent3.getFilename()).getCurrentState());
        FileEvent fileEvent4=new FileEvent("test", WatchedFile.Symbol.MODIFY);
        Test.update(fileEvent4);
        assertEquals(WatchedFile.State.MODIFIED,Test.getMap().get(fileEvent4.getFilename()).getCurrentState());

    }



    @org.junit.Test
    public void sync() throws IOException {
        FileEvent fileEvent=new FileEvent("test", WatchedFile.Symbol.CREATE);
        Test.update(fileEvent);
        Test.sync(System.out);
        System.out.println("");
        Test.sync(System.out);
    }


    @org.junit.Test
    public void getDirectionary() {
        assertEquals(Test.getDirectionary(),"verzeichnisname");
    }
}