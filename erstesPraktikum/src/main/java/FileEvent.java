public class FileEvent {
    private String Filename;
    private WatchedFile.Symbol Symbol;

    public WatchedFile.Symbol getSymbol() {
        return Symbol;
    }

    public String getFilename() {
        return Filename;
    }

    FileEvent(String Filename, WatchedFile.Symbol symbol) {
        this.Filename = Filename;
        this.Symbol = symbol;
    }
}
