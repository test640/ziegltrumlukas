

import java.time.Instant;
import java.util.Date;

public class WatchedFile {
    private Date lastTimstamp;
    private String Dateiname;
    private String directory;

    enum State {INSYNC, CREATED, MODIFIED, DELETED, GONE}

    ;

    enum Symbol {CREATE, DELETE, SYNC, MODIFY}

    //      INSYNC          CREATED,      MODIFIED,       DELETED,          GONE
    State[][] transitionTable = {
            {State.MODIFIED, State.CREATED, State.MODIFIED, State.MODIFIED, State.CREATED},//Creat
            {State.DELETED, State.GONE, State.DELETED, State.DELETED, State.GONE},//DELETE
            {State.INSYNC, State.INSYNC, State.INSYNC, State.GONE, State.GONE},//SYNC
            {State.MODIFIED, State.CREATED, State.MODIFIED, State.MODIFIED, State.GONE},//MODIFY
    };
    private State currentState = State.CREATED;

    WatchedFile(String filename, String directory) {
        lastTimstamp = Date.from(Instant.now());
        this.Dateiname = filename;
        this.directory =  directory;
    }

    public String getVerzeichnis() {
        return directory;
    }

    public String getFilename() {
        return Dateiname;
    }

    public State getCurrentState() {
        return currentState;
    }

    public Date getLastTimstamp() {
        return lastTimstamp;
    }

    public void transition(Symbol symbol) {
        currentState = transitionTable[symbol.ordinal()][currentState.ordinal()];
        lastTimstamp = Date.from(Instant.now());
    }

    @Override
    public String toString() {
        return " Die Datei "+getFilename()+" im Verzeichnis "+getVerzeichnis()+" wurde Zum Zeitpunkt "+getLastTimstamp()+" "+getCurrentState();
    }
}
