import java.io.IOException;

import java.io.OutputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.logging.Level;

public class SyncSever {
   // protected OutputStream out= System.out;
    private static final int port =1337;
    protected void start(WatchedDirectory watchedDirectory)
    {try (ServerSocket socket= new ServerSocket(port)){
        Executor exe = Executors.newFixedThreadPool(5);
        DirectoryWatcher.logger.log(Level.INFO,"Sever wurde gestartet");
        while (!Thread.currentThread().isInterrupted()){
           Socket client = socket.accept();
           exe.execute(() -> work(client,watchedDirectory));

        }

    }catch (IOException e)  {
       DirectoryWatcher.logger.log(Level.SEVERE, "Fehler beim Starten des Severs");
    }

    }
   protected void work(Socket client,WatchedDirectory watchedDirectory)
    {DirectoryWatcher.logger.log(Level.INFO,"Client ist mit dem Sever verbunden"+client.getPort());
    try (PrintStream toclient = new PrintStream(client.getOutputStream())){
        while (!Thread.currentThread().isInterrupted()) {
            Thread.currentThread().sleep(20000);
            watchedDirectory.sync(toclient);
            toclient.println();

        }

    }catch (IOException e)  {
        DirectoryWatcher.logger.log(Level.SEVERE, "Fehler beim arbeiten(IOexception");
        Thread.getDefaultUncaughtExceptionHandler();
    } catch (InterruptedException e) {
        DirectoryWatcher.logger.log(Level.SEVERE, "Fehler beim arbeiten(Interupted exception)");
        Thread.getDefaultUncaughtExceptionHandler();
    }

    }
}
