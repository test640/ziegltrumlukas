
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.*;
import java.util.concurrent.*;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;


public class DirectoryWatcher {
    private WatchService WatchService;
    private WatchKey watchKey;
    private Path path;
    protected static Logger logger = Logger.getLogger(DirectoryWatcher.class.getPackage().getName());
   private BlockingQueue<FileEvent> queue = new ArrayBlockingQueue<>(1000);
    OutputStream outputStream =System.out;




    DirectoryWatcher(String args) {
        start(args);
    }

    protected FileEvent neuesFileEvent(WatchEvent watchEvent) {

        FileEvent returnvalue;
        switch (watchEvent.kind().name()) {
            case "ENTRY_CREATE":
                returnvalue = new FileEvent(watchEvent.context().toString(), WatchedFile.Symbol.CREATE);
                break;
            case "ENTRY_DELETE":
                returnvalue = new FileEvent(watchEvent.context().toString(), WatchedFile.Symbol.DELETE);
                break;
            case "ENTRY_MODIFY":
                returnvalue = new FileEvent(watchEvent.context().toString(), WatchedFile.Symbol.MODIFY);
                break;
            default:
                returnvalue = null;

        }

        return returnvalue;

    }

    private void start(String args) {
        try {
            //Watchservice erstellen
            path = FileSystems.getDefault().getPath(args);
            WatchService = path.getFileSystem().newWatchService();

        } catch (IOException e) {
            logger.log(Level.SEVERE, "Fehler beim Erstellen von einem Watchservice");
           Thread.getDefaultUncaughtExceptionHandler();
        }

        try {
            //Watchkey erstellen und Watchevents regristrieren
            watchKey = path.register(WatchService, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE, StandardWatchEventKinds.ENTRY_MODIFY);
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Fehler beim regstrieren des Watchkeys");
            Thread.getDefaultUncaughtExceptionHandler();
        }
    }

    public java.nio.file.WatchService getWatchService() {
        return WatchService;
    }

    public static void main(String[] args) {
        ExecutorService exe = Executors.newCachedThreadPool();
        String parameter;
        FileHandler handler;

        try { handler = new FileHandler(DirectoryWatcher.class.getName() + ".log");
            handler.setFormatter(new SimpleFormatter());
            logger.addHandler(handler);
            logger.setUseParentHandlers(false);

        } catch (IOException e) {
            System.out.println("Logfile " + DirectoryWatcher.class.getName() + ".log" + "not accessible");
            Thread.getDefaultUncaughtExceptionHandler();
        }
        //Ausortieren ob KomandoZielen Parameter existiert
        if (args.length != 0)
            parameter = args[0];
        else
            parameter = System.getProperty("user.dir");
        DirectoryWatcher directoryWatcher = new DirectoryWatcher(parameter);
        Runnable directoryWatcherThread = () -> {
            try {
                while (!Thread.currentThread().isInterrupted()) {
                    directoryWatcher.watchKey = directoryWatcher.WatchService.take();
                    //meine Lieblingszeile im gesmaten programm.Es wird ein neus File Event Der blockqueue übergeben. Watch event ist das letzte das übergeben wurde
                    directoryWatcher.queue.add(directoryWatcher.neuesFileEvent(directoryWatcher.watchKey.pollEvents().get(0)));
                    directoryWatcher.watchKey.reset();
                }
            } catch (InterruptedException e) {
                logger.log(Level.SEVERE, "Der directoryWatcherThread wurde interupted");
                Thread.getDefaultUncaughtExceptionHandler();
            }
        };
        WatchedDirectory watchedDirectory = new WatchedDirectory(directoryWatcher.path.toString());
        Runnable watchedDirectoryThread = () -> {
            try {
                while (!Thread.currentThread().isInterrupted()) {
                    watchedDirectory.update(directoryWatcher.queue.take());
                }
            } catch (InterruptedException e) {
                logger.log(Level.SEVERE, "Der watchedDirectoryThread wurde interupted");
                Thread.getDefaultUncaughtExceptionHandler();
            }
        };
        Runnable syncThread = () -> {
            try {
                while (!Thread.currentThread().isInterrupted()) {
                    Thread.currentThread().sleep(10000);
                    watchedDirectory.sync(directoryWatcher.outputStream);
                    System.out.println();

                }
            } catch (InterruptedException e ) {
                logger.log(Level.SEVERE, "Der syncThread wurde interupted");
                Thread.getDefaultUncaughtExceptionHandler();
            }
        };
        Runnable ServerThread = () -> {
            SyncSever syncSever =new SyncSever();
            syncSever.start(watchedDirectory);
        };
        exe.execute(ServerThread);
        exe.execute(syncThread);
        exe.execute(directoryWatcherThread);
        exe.execute(watchedDirectoryThread);


    }


}




