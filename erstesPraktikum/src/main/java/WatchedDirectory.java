import com.google.gson.*;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class WatchedDirectory {

    FileHandler handler;

    {
        try {
            handler = new FileHandler(WatchedDirectory.class.getName() + ".log");
        } catch (IOException e) {
            e.printStackTrace();
            handler.setFormatter(new SimpleFormatter());
            DirectoryWatcher.logger.addHandler(handler);
            DirectoryWatcher.logger.setUseParentHandlers(false);
        }
    }

    private Map<String, WatchedFile> map = new HashMap<String, WatchedFile>();
    private String directionary;
    private JsonSerializer<Map<String, WatchedFile>> jsonSerializer = (src, typeOfSrc, context) -> new JsonPrimitive(src.toString());
    Gson gsonBuilder = new GsonBuilder().registerTypeAdapter(Map.class, jsonSerializer).create();


    WatchedDirectory(String directionary) {
        this.directionary = directionary;
    }

    public void update(FileEvent fileEvent) {
        if (map.get(fileEvent.getFilename()) != null)//Es wird geprüft ob Datei schon in Map drin ist
            map.get(fileEvent.getFilename()).transition(fileEvent.getSymbol());
        else {
            map.put(fileEvent.getFilename(), new WatchedFile(fileEvent.getFilename(),directionary));
            map.get(fileEvent.getFilename()).transition(fileEvent.getSymbol());
        }
    }

    public void sync(OutputStream out) {
        for (Map.Entry<String, WatchedFile> smap : map.entrySet())// jeder Eintrag wird durchgegangen
        {
            String gsonToJason = gsonBuilder.toJson(smap.getValue().toString());
            try {
                out.write(gsonToJason.getBytes());
                out.flush();
            } catch (IOException e) {
                DirectoryWatcher.logger.log(Level.SEVERE, "IOException Beim Schreiben in Outputstream");
                Thread.getDefaultUncaughtExceptionHandler();
            }
            map.get(smap.getKey()).transition(WatchedFile.Symbol.SYNC);//Zustand wird au Sync gesetzt
        }
    }

    public Map<String, WatchedFile> getMap() {
        return map;
    }

    public String getDirectionary() {
        return directionary;
    }
}

