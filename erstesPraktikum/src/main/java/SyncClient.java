import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;
import java.util.logging.Level;

public class SyncClient {

    public static void main(String[] args) {
        try (Socket socket = new Socket(InetAddress.getLocalHost(),1337) ){
            Scanner scanner = new Scanner(socket.getInputStream());
            while(!Thread.currentThread().isInterrupted()) {
                String output = scanner.nextLine();
                System.out.println(output);
            }

        } catch (UnknownHostException e) {
          DirectoryWatcher.logger.log(Level.SEVERE,"UnknownHostException beim Client");
            Thread.getDefaultUncaughtExceptionHandler();
        } catch (IOException e) {
            DirectoryWatcher.logger.log(Level.SEVERE,"IOException beim Client");
            Thread.getDefaultUncaughtExceptionHandler();
        }



    }


}