package roman;

public class number {
    /**
     * @param number
     * @return
     */
    static int romantoarabic(String number) {
        int resultierend = 0;
        int prev = 0;
        if (number == null)
            return resultierend;
        else {
            switch (number.charAt(0)) {
                case 'I':
                    prev = 1;
                    break;
                case 'V':
                    prev = 5;
                    break;
                case 'X':
                    prev = 10;
                    break;
                case 'L':
                    prev = 50;
                    break;
                case 'C':
                    prev = 100;
                    break;
                case 'D':
                    prev = 500;
                    break;
                case 'M':
                    prev = 1000;
                    break;

            }
            for (int i = 1; i < number.length(); i++) {
                switch (number.charAt(i)) {
                    case 'I':
                        resultierend += prev;
                        prev = 1;
                        break;
                    case 'V':
                        if (prev < 5){
                            resultierend = 5 - prev + resultierend;
                        prev=0;}
                        else{
                            resultierend += prev;
                            prev = 5;
                        }
                        break;

                    case 'X':
                        if (prev < 10){
                            resultierend = 10 - prev + resultierend;
                        prev=0;}
                        else{
                            resultierend += prev;
                            prev = 10;
                        }
                        break;
                    case 'L':
                        if (prev < 50){
                            resultierend = 50 - prev + resultierend;
                        prev=0;}
                        else{
                            resultierend += prev;
                            prev = 50;
                        }
                        break;
                    case 'C':
                        if (prev < 100){
                            resultierend = 100 - prev + resultierend;
                        prev=0;}
                        else{
                            resultierend += prev;
                            prev = 100;
                        }
                        break;
                    case 'D':
                        if (prev < 500){
                            resultierend = 500 - prev + resultierend;
                        prev=0;}
                        else{
                            resultierend += prev;
                            prev = 500;
                        }
                        break;
                    case 'M':
                        if (prev < 1000){
                            resultierend = 1000 - prev + resultierend;
                        prev=0;}
                        else{
                            resultierend += prev;
                            prev = 1000;
                        }
                        break;

                }


            }
        }
        resultierend+= prev;
        return resultierend;
    }

    public static void main(String[] args) {
        System.out.println(romantoarabic("MCCCXXXVII"));
    }
}


